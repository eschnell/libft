/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strspn.c                                      .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: eschnell <eschnell@student.le-101.fr>      +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/07/16 16:53:28 by raging       #+#   ##    ##    #+#       */
/*   Updated: 2019/10/06 15:52:49 by eschnell    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"

size_t	ft_strspn(const char *s, const char *charset)
{
	t_bool			not_found;
	unsigned int	i;
	unsigned int	j;
	unsigned int	k;

	j = 0;
	k = 0;
	while (*(s + j))
	{
		i = 0;
		not_found = true;
		while (*(charset + i))
		{
			if (*(charset + i) == *(s + j))
			{
				k++;
				not_found = false;
			}
			i++;
		}
		if (not_found)
			break ;
		j++;
	}
	return (k);
}
