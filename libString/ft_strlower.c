/* ************************************************************************** */
/*                                                          LE - /            */
/*                                                              /             */
/*   ft_strlower.c                                    .::    .:/ .      .::   */
/*                                                 +:+:+   +:    +:  +:+:+    */
/*   By: eschnell <eschnell@student.le-101.fr>      +:+   +:    +:    +:+     */
/*                                                 #+#   #+    #+    #+#      */
/*   Created: 2019/10/09 22:16:55 by eschnell     #+#   ##    ##    #+#       */
/*   Updated: 2019/10/09 22:50:57 by eschnell    ###    #+. /#+    ###.fr     */
/*                                                         /                  */
/*                                                        /                   */
/* ************************************************************************** */

#include "libft.h"
#include <stdio.h>
#include <stdlib.h>

char		*ft_strlower(char *s)
{
	char		*tmp;
	u_int32_t	i;

	i = 0;
	tmp = ft_strdup(s);
	while (tmp[i])
	{
		if (ft_isupper(tmp[i]))
			tmp[i] += 32;
		i++;
	}
	return (tmp);
}
