# LIBFT PROJECT

# 

[![pipeline status](https://gitlab.com/eschnell/libft/badges/master/pipeline.svg)](https://gitlab.com/eschnell/libft/commits/master)	[![coverage report](https://gitlab.com/eschnell/libft/badges/master/coverage.svg)](https://gitlab.com/eschnell/libft/commits/master)

# 

* ***Requirements***
	- gcc
	- glibc
	- GNU make

* ***To clone and compile***
	- `git clone https://gitlab.com/eschnell/libft.git`
	- `cd libft && make all`
