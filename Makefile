# **************************************************************************** #
#                                                           LE - /             #
#                                                               /              #
#    Makefile                                         .::    .:/ .      .::    #
#                                                  +:+:+   +:    +:  +:+:+     #
#    By: eschnell <eschnell@student.le-101.fr>      +:+   +:    +:    +:+      #
#                                                  #+#   #+    #+    #+#       #
#    Created: 2018/11/14 17:47:09 by eschnell     #+#   ##    ##    #+#        #
#    Updated: 2019/10/06 15:55:42 by eschnell    ###    #+. /#+    ###.fr      #
#                                                          /                   #
#                                                         /                    #
# **************************************************************************** #

NAME	= libft.a
DNAME	= libft.so
CC		= gcc
FLAGS	= -Wall -Werror -Wextra
CFLAGS	= $(FLAGS) -Iincludes
subdirs := $(wildcard lib*/)
FILES	:= $(wildcard $(addsuffix *.c,$(subdirs)))
OBJS	:= $(patsubst %.c,%.o,$(FILES))
TESTS	:= $(wildcard tests/*.c)

all: $(NAME)

# This won't run if the .o files don't exist or are not modified
#so
$(NAME): $(OBJS)
	@ar rcs $(NAME) $(shell find . -name "*.o")
	@ranlib $(NAME)
	@echo -e \# Static libft.a
	@echo "\033[32m[✓] LIBFT COMPILED\033[0m"

so:
	gcc -shared -fpic -o libft.so *.o -lc

test: $(NAME) $(OBJS) $(TESTS)
	@mkdir -p tests
	@touch tests/main.c
	@gcc $(CFLAGS) $(TESTS) libft.a -o test

$(OBJ): %.o : %.c

clean:
	rm -f lib*/*.o
	@echo "\033[31m[✘] object files deleted\033[0m"

fclean: clean
	rm -f $(NAME) $(DNAME)
	@echo "\033[31m[✘] $(NAME) && $(DNAME) deleted\033[0m"

re: fclean all

.PHONY: clean fclean all re
